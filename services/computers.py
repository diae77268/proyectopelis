from models.computers import Computers as ComputerModel
from schemas.computers import Computers

class ComputerService():

    def __init__(self, db) -> None:
        self.db = db

    def get_computers(self):
        result = self.db.query(ComputerModel).all()
        return result
    
    def get_computer(self, id):
        result = self.db.query(ComputerModel).filter(ComputerModel.id == id).first()
        return result
    
    def get_computers_by_marca(self, marca):
        results = self.db.query(ComputerModel).filter(ComputerModel.marca == marca).all()
        return results
    
    def create_computer(self, computer: Computers):
        new_computer = ComputerModel(**computer.model_dump())
        self.db.add(new_computer)
        self.db.commit()
        return
    
    def update_computer(self, id: int, data: Computers):
        computer = self.db.query(ComputerModel).filter(ComputerModel.id == id).first()
        computer.marca = data.marca
        computer.modelo = data.modelo
        computer.color = data.color
        computer.ram = data.ram
        computer.almacenamiento = data.almacenamiento
        self.db.commit()
        return
    
    def delete_computer(self, id: int):
        self.db.query(ComputerModel).filter(ComputerModel.id == id).delete()
        self.db.commit()
        return
