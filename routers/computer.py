from fastapi import Path, Query, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.computers import Computers as ComputerModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from fastapi import APIRouter
from services.computers import ComputerService
from schemas.computers import Computers

computer_router = APIRouter()

@computer_router.get('/computers', tags=['Computadoras'], response_model= List[Computers], status_code=200)
def get_computers() -> List[Computers]:
    db = Session()
    result = ComputerService(db).get_computers()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@computer_router.get('/computers/{id}', tags=['Computadoras'], response_model= Computers)
def get_computer(id: int = Path(ge=1, le=2000)) -> Computers:
    db = Session()
    result = ComputerService(db).get_computer(id)
    if not result:
        return JSONResponse(status_code=404, content={'message': "No encontrado"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@computer_router.get('/Computers/', tags=['Computadoras'], response_model= List[Computers])
def get_computers_by_marca(marca: str = Query(min_length=5, max_length=15)):
    db = Session()
    result = ComputerService(db).get_computers_by_marca(marca)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@computer_router.post('/computers', tags=['Computadoras'], response_model= dict, status_code=200)
def create_computer(computer : Computers) -> dict:
    db = Session()
    ComputerService(db).create_computer(computer)
    return JSONResponse(status_code=200, content={"message": "Se registro la computadora"})

@computer_router.put('/computers/{id}', tags=['Computadoras'], response_model= dict, status_code=200)
def update_computer(id: int, computer: Computers) -> dict:
    db = Session()
    result = ComputerService(db).get_computer(id)
    if not result:
        return JSONResponse(status_code= 404, content={'message': "No encontrado"})
    
    ComputerService(db).update_computer(id, computer)
    return JSONResponse(status_code=202, content={'message': "Se ha modificado la computadora"})
        
@computer_router.delete('/computers/{id}', tags=['Computadoras'], response_model= dict, status_code=200)
def delete_computer(id: int) -> dict:
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(status_code= 404, content={'message': "No encontrado"})
    ComputerService(db).delete_computer(id)
    return JSONResponse(status_code=200, content={"message": "Se ha Eliminado la computadora"})
