from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.user import user_router
from routers.computer import computer_router

app = FastAPI()
app.title = "Mi primera chamba"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(computer_router)
app.include_router(user_router)

Base.metadata.create_all(bind=engine)

@app.get('/', tags=['Inicio'])
def message():
    return HTMLResponse('<h1> Hola Mundo </h1>')
