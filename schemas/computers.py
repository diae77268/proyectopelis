from pydantic import BaseModel, Field
from typing import Optional

class Computers(BaseModel):
    id: Optional[int] = None
    marca: str = Field(min_length = 2, max_length=50)
    modelo: str = Field(min_length= 2, max_length=50)
    color: str = Field(min_length= 2, max_length=50)
    ram: int = Field(ge= 2, le=254)
    almacenamiento: int = Field(ge= 2, le=100000)

    class Config:
        json_schema_extra = {
            "example": {
                "id": 1,
                "marca": "Apple",
                "modelo": "Macbook Air M2",
                "color": "Gris Espacial",
                "ram": 8,
                "almacenamiento": 256
            }
        }
